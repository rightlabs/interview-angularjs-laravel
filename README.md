``` ______     __     ______     __  __     ______   __         ______     ______     ______       
/\  == \   /\ \   /\  ___\   /\ \_\ \   /\__  _\ /\ \       /\  __ \   /\  == \   /\  ___\      
\ \  __<   \ \ \  \ \ \__ \  \ \  __ \  \/_/\ \/ \ \ \____  \ \  __ \  \ \  __<   \ \___  \     
 \ \_\ \_\  \ \_\  \ \_____\  \ \_\ \_\    \ \_\  \ \_____\  \ \_\ \_\  \ \_____\  \/\_____\    
  \/_/ /_/   \/_/   \/_____/   \/_/\/_/     \/_/   \/_____/   \/_/\/_/   \/_____/   \/_____/    
                                                                                                
 __     __   __     ______   ______     ______     __   __   __     ______     __     __        
/\ \   /\ "-.\ \   /\__  _\ /\  ___\   /\  == \   /\ \ / /  /\ \   /\  ___\   /\ \  _ \ \       
\ \ \  \ \ \-.  \  \/_/\ \/ \ \  __\   \ \  __<   \ \ \'/   \ \ \  \ \  __\   \ \ \/ ".\ \      
 \ \_\  \ \_\\"\_\    \ \_\  \ \_____\  \ \_\ \_\  \ \__|    \ \_\  \ \_____\  \ \__/".~\_\     
  \/_/   \/_/ \/_/     \/_/   \/_____/   \/_/ /_/   \/_/      \/_/   \/_____/   \/_/   \/_/     
                                                                                                
 ______   ______     ______     ______                                                          
/\__  _\ /\  ___\   /\  ___\   /\__  _\                                                         
\/_/\ \/ \ \  __\   \ \___  \  \/_/\ \/                                                         
   \ \_\  \ \_____\  \/\_____\    \ \_\                                                         
    \/_/   \/_____/   \/_____/     \/_/   

```

## Important - Before you begin
- The AngularJS / NodeJS test evaluates the candidate's creativity, problem-solving, as well as knowledge of AngularJS 1.6 and NodeJS (the stack used for ActivityRight)
- The test below reflects majority of our codebase in ActivityRight. Therefore, accomplishing all the tasks correctly would make the candidate suitable for the project
- The tasks are generic and therefore, unless explicitly noted, the tasks can be solved in many different ways and will be evaluated based on the problem-solving approach
- If any of the items are unclear or you want to ask anything about the test, feel free to contact your interviewer (the one who sent you this test).


## Part 1 - AngularJS Tasks

**Notes**
- We're using Laravel 5.4 PHP Framework to power the backend engine for this test - knowledge of Laravel is not required for this test
- The main file (index) for the AngularJS application is inside **resources/views/app.php**
- All the AngularJS files that you would need to work with are located in **public/frontend**
- For any css that you do, you can create your own classes but extra points if you follow 100% the **AngularJS Material Design** patterns
- To figure out which parameters are being passed into the Laravel controllers, take a look at the files under **app/Http/Controllers**
- To understand how the routes are written in Laravel, take a look at **routes/api.php** and **routes/web.php**
- The test had been pre-populated with sample data in the database

### Lazy Loading and filters
* [ ] Add lazy loading when scrolling down the Announcement list. Currently, all the announcement cards are loaded when user loads http://<ip_address>/app. 
  - The goal is to trigger lazy loading as soon as the user scrolls down to load the next set of announcements
  - **You must** create a directive that will check the size of the container viewport that holds the announcement cards. If user goes past the bounds (vertically) of the container viewport, trigger a callback to the **loadAnnouncement()**
  - **loadAnnouncement()** is located under **public/frontend/js/activities.module/components/announcements.component.js**

* [ ] Add search capability for the announcements list
  - The goal is to trigger a filter of the list as the user types something in the **Search by title** field

* [ ] Add functionality to the refresh button to reload the announcements list. Make sure that if user has a **searchText** value, it has to be kept in the query as well so that the reloaded list will have the filter applied automatically

* [ ] After lazy loading is implemented, the search bar has to be **fixed position** and the only scrollable area would be the announcement list


### Announcement Setup Component - public/frontend/templates/activities.module/announcement-setup.component.html

* [ ] Create all the logic for the Create | Update | Delete Announcement - this will involve knowledge of forms, services, and http calls to the backend

* [ ] Create a form for the title and content ( add validations to the title, and content )

* [ ] Create a sub form for the participants lists (you are free to use a list, a table, chips or any easy-to-use UX you might come up with)
  * [ ] You should be able to select multiple participants that will receive this announcement
  * [ ]  Validate that the announcement must have at least 1 participant

* [ ] Add lazy loading for the participants list ( load 20 by 20 ) ( if you decided to use chips, they have to also be loaded async.)

* [ ] When updating an announcement, check first if anything has changed. If nothing changed, do not send an update to the server.

* [ ] To save an announcement you must:
  * [ ] First save the announcement, and then 
  * [ ] Save the linked participants via nested promises (not the optimal way but it's for the purposes of this test only).

Required buttons at the end of the Announcement form:
- Delete ( only if you have an ID )
- Save ( disable if validations didn't pass )
- Cancel ( this will redirect back to the list )


### Announcement Card Component - public/frontend/templates/activities.module/announcement-card.component.html

* [ ] When you click on the announcement title, you will also redirect to the `app.announcement` state with `:id = {{announcement.announcementId}}`

* [ ] Display the participants count in the announcementCardComponent

* [ ] Enforce one-time binding for [title, content, updated_at, participantsCount]


### Bonus (Optional)

* [ ] Change the announcements.service to use the http.service.js instead of calling directly the $http angularJS service. 

* [ ] Add validations to the http.service.js for all the HTTP verbs.

* [ ] Make it fully responsive for Mobile devices ( iphone 5 as minimum - 320px width )


## Part 2 - NodeJS Tasks

**Notes**
- Due to the addition of NodeJS into our primary tech stack, we'd also like to assess your capabilities in NodeJS.
- To test this, we added the `node-version` folder under the same project
- All the tasks below are NOT related to **Part1** of this test and all the files you need to work with are under the `node-version` folder (except that the database is shared on both)
- To setup the `node-version`, you should run the following commands in the terminal
```
cd node-version/back-end
npm install
cd ..
cd front-end
npm install
```

- You can decide which port (NODE_PORT) for the ``back-end`` by updating ``.env`` under ``back-end`` folder

- To run both HTTP and NodeJS servers, you'll have to run these commands in two separate terminals
  - Under `node-version/back-end` folder: `npm run start`
  - Under `node-version/front-end` folder: `npm run start`

### Tasks
* [ ] You should be able to run both servers and render them in your browser.
  - If in ``back-end``, you used NODE_PORT=4040, accessing http://<ip_address>:4040 should render a JSON that looked like this: ```{"hello":"World -:>"}```  
  - You should be able to access ``front-end`` by loading http://<ip_address>:8000 and render the Announcement list

* [ ] Create a nodejs rest API that will read all the Announcements

* [ ] Update the frontend to fetch all the Announcements from the API created above and display it in the Announcements list

```
Congratulations, you're done!
```


### Bonus (Optional)
These requirements are completely optional, but doing any them will give us a better understanding of your mastery of nodejs.

* [ ] Create the Routes to CRUD Announcements

* [ ] Create the Announcement Controller that will get information from the Request and pass it to the Announcement Module ( Business logic inside that module )

* [ ] Create the Routes to CRUD participants

* [ ] Create the Participant Controller that will get information from the Request and pass it to the Participant Module ( Business logic inside that module )

* [ ] Create a Sign in screen

* [ ] Create a Sign up screen

* [ ] Implement a simple JWT Authentication

* [ ] Make sure the Announcements only displays the ones that you created

* [ ] Endpoints should be protected - unauthorized users that don't have valid JWT will not be able to access any resource

* [ ] Make sure all the frontend and the backend are connected and pulling the data properly ( render the announcements list, lazy loading, etc )

* [ ] Announcements should bring the relationship with the participants in the same query using Sequelize ORM
