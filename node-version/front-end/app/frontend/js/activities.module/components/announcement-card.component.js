

var announcementCardComponent = {
    bindings: {
        announcement: '<'  
    },
    templateUrl: 'frontend/templates/activities.module/announcement-card.component.html',
    controller: announcementCardController,
    controllerAs: 'announcementCardCtrl'
};

announcementCardController.$inject = [];

function announcementCardController() {
    
    var vm = this;
    
    
    vm.$onInit = function() {
        
        console.log('announcementCardController loaded', vm.announcement);
        
    };
    
    
}
