require('dotenv').config();

const API_ROOT = '/node-api';

module.exports = {
    name: 'API',
    env: process.env.NODE_ENV || 'development',
    port: process.env.NODE_PORT || 4040,
    base_url: process.env.BASE_URL || 'http://node-api.dev',
    db: {
        uri: process.env.MONGODB_URI || 'mongodb://127.0.0.1:27017/api',
    },
    mysql: {
        database: process.env.MYSQL_DATABASE || 'homestead',
        username: process.env.MYSQL_DATABASE_USERNAME || 'homestead',
        password: process.env.MYSQL_DATABASE_PASSWORD || 'secret',
        host: process.env.MYSQL_DATABASE_HOST || '192.168.10.10',
        port: process.env.MYSQL_DATABASE_PORT || 3306,
        dialect: 'mysql',
        operatorsAliases: process.env.MYSQL_DATABASE_OPERATORS_ALIASES || false,
        pool: {
            max: process.env.MYSQL_POOL_MAX,
            min: process.env.MYSQL_POOL_MIN,
            acquire: process.env.MYSQL_POOL_ACQUIRE,
            idle: process.env.MYSQL_POOL_IDLE,
        },
    },
    // key to generate/verify JWT
    JWT_SECRET: process.env.JWT_SECRET,
    // will be used to building route paths
    basePath: (path) => {
        return API_ROOT.replace(/\/$/, '') + '/' + path.replace(/^\//, '');
    },
};

console.log(`Server on MODE  [${process.env.NODE_ENV}]`);


/**
 * mongo db , Schema columns types:
 *
 *      String.
        Number.
        Date.
        Buffer.
        Boolean.
        Mixed.
        ObjectId.
        Array.
 *
 */
