module.exports = (server) => {
    const PATH = '/';

    server.get({
        path: PATH,
        version: '1.0.0',
    }, function (request, response, next) {

        response.send({
            hello: `World -:> ${request.params.name || ''}`
        });
        return next();
    });
};
