<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnnouncementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $this->verb = $this->route()->methods()[0];
          
        switch ($this->verb) {
            
            case 'POST':
            
            case 'PUT':
            
                if (strpos($this->route()->uri(), '/participants')) {
                    
                    return [
                        'participantIds' => 'required | array',
                        'participantIds.*' => 'integer'   
                    ];
                    
                }    
                
                return [
                    'title' => 'string | required',
                    'content' => 'string | required'
                ];
                
                break;
            
            case 'GET':
            case 'DELETE':
        
                return [];
            
        }
    }
}
