<?php

use Illuminate\Database\Seeder;

use App\Participant;

use Faker\Factory as Faker;

class ParticipantsSeeder extends Seeder
{
    /**
     * Run the database seeds for the Participants.
     *
     * @return void
     */
    public function run()
    {
        
        $faker = Faker::create();
        
        foreach (range(1,50) as $index) {
	        
	        $participant = new Participant();
	        
	        $participant->fill([
	            'firstName' => $faker->firstName,
	            'lastName' => $faker->lastName,
	            'email' => $faker->email
	        ]);
	           
	       $participant->save();
            
        }
        

    }
}
