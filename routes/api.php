<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/announcements', 'AnnouncementsController@index');

Route::get('/announcements/{announcementId}', 'AnnouncementsController@show');

Route::post('/announcements', 'AnnouncementsController@store');

Route::put('/announcements/{announcementId}', 'AnnouncementsController@update');

Route::put('/announcements/{announcementId}/participants', 'AnnouncementsController@attachParticipants');

Route::delete('/announcements/{announcementId}', 'AnnouncementsController@destroy');

Route::get('/participants', 'ParticipantsController@index');